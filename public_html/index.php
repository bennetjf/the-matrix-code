<html>
<head>
	<title>th3M@tr1xH@sY0u</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<style>
		body {
			font-family: Monospace;
			background-color: #f0f0f0;
			margin: 0px;
			overflow: hidden;
		}
	</style>
</head>
<body>
	<script src="/js/three.min.js"></script>
	<script src="/js/tween.min.js"></script>
	<script src="/js/stats.min.js"></script>

	<script>

		var container, stats;

		var camera, scene, renderer;
		var controls, group;

		init();
		animate();

		function init() {
			scene = new THREE.Scene();
			camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
			camera.position.set(0, 0, 0);
			renderer = new THREE.WebGLRenderer({
			  alpha: true // remove canvas' bg color
			});
			renderer.setSize(window.innerWidth, window.innerHeight);
		}

		function onWindowResize() {

			camera.aspect = window.innerWidth / window.innerHeight;
			camera.updateProjectionMatrix();

			renderer.setSize( window.innerWidth, window.innerHeight );

		}

		//

		function animate() {


		}

		function render() {

			

		}

	</script>

</body>
</html>