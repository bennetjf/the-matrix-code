
var example = (function (){
    "use strict";
    var stats = new Stats();
    var scene = new THREE.Scene();
    var renderer = new THREE.WebGLRenderer();
    var light = new THREE.AmbientLight(0xFF00FF);
    var camera = new THREE.PerspectiveCamera(
        75, window.innerWidth / window.innerHeight, 1, 100
    );
    // var box = new THREE.Mesh(
    //     new THREE.BoxGeometry(20, 30, 40),
    //     new THREE.MeshBasicMaterial({color: 0xFF0000})
    // );
    var loader = new THREE.ColladaLoader();
    var monkey;

    function initScene(){
        renderer.setSize( window.innerWidth, window.innerHeight );
        document.getElementById('threeD').appendChild ( renderer.domElement );

        scene.add( light );
        
        camera.position.z = 100;
        scene.add( camera );

        //box.name = 'box';
        //scene.add(box);
        loader.load('models/monkey.dae', function( collada ){
            monkey = collada.scene;
            scene.add(monkey);
            //render();
        });
        

        stats.domElement.style.position = 'absolute';
        stats.domElement.style.left = 0;
        stats.domElement.style.top = 0;
        document.body.appendChild(stats.domElement);

        render();

    }

    function render () {
        // box.rotation.x += 0.01;
        // box.rotation.y += 0.001;
        // box.rotation.z += 0.0001;

        stats.update();

        renderer.render(scene, camera);
        requestAnimationFrame ( render );
    }

    window.onload = initScene;

    return {
        scene: scene
    }

})();